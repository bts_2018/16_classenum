
public class main {

	public static void main(String[] args) {
		
		//case 1
		for (Season s : Season.values()) {
			System.out.println(s);
		}

		//case 2
		for (Directions dir : Directions.values())  {

			switch (dir) {
			case EAST:
				System.out.println("In East Direction");
				break;                 
			case WEST:
				System.out.println("In West Direction");
				break;         
			case NORTH: 
				System.out.println("In North Direction");
				break;  
			default:
				System.out.println("In South Direction");
				break;
			}
		}
		
		
		Directions x = Directions.EAST;
		System.out.println(x);

	}

}
